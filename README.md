# traceable 

[![CircleCI](https://circleci.com/gh/ConorNevin/traceable/tree/main.svg?style=svg)](https://circleci.com/gh/ConorNevin/traceable/tree/main)
[![Coverage Status](https://coveralls.io/repos/github/ConorNevin/traceable/badge.svg?branch=main)](https://coveralls.io/github/ConorNevin/traceable?branch=main)
[![Go Report Card](https://goreportcard.com/badge/github.com/ConorNevin/traceable)](https://goreportcard.com/report/github.com/ConorNevin/traceable)
[![GoDoc](https://godoc.org/github.com/ConorNevin/traceable?status.svg)](https://godoc.org/github.com/ConorNevin/traceable)

A Tool that generates an instrumented implementation of an interface that wraps functions calls with an OpenTracing span.

## How it works

## Installation

## Usage